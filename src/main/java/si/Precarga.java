package si;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import si.model.Mesa;
import si.repositories.MesaRepository;

@Configuration
public class Precarga {


    @Bean
    public CommandLineRunner chargeMesa(MesaRepository mesaRepository) {
        return (args -> {

            Mesa mesa = new Mesa(1,4);
            Mesa mesa1 = new Mesa(2,3);
            Mesa mesa2 = new Mesa(3,8);
            Mesa mesa3 = new Mesa(4,10);
            Mesa mesa4 = new Mesa(5,10);
            Mesa mesa5 = new Mesa(6,2);
            Mesa mesa6 = new Mesa(7,4);
            Mesa mesa7 = new Mesa(8,8);
            Mesa mesa8 = new Mesa(9,2);
            Mesa mesa9 = new Mesa(10,2);
            Mesa mesa10 = new Mesa(11,10);
            Mesa mesa11 = new Mesa(12,10);

            mesaRepository.save(mesa);
            mesaRepository.save(mesa1);
            mesaRepository.save(mesa2);
            mesaRepository.save(mesa3);
            mesaRepository.save(mesa4);
            mesaRepository.save(mesa5);
            mesaRepository.save(mesa6);
            mesaRepository.save(mesa7);
            mesaRepository.save(mesa8);
            mesaRepository.save(mesa9);
            mesaRepository.save(mesa10);
            mesaRepository.save(mesa11);
        });
    }

}
