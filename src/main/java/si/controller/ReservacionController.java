package si.controller;

import si.model.Reservacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import si.services.ReservacionService;

import java.util.List;

@RestController
@RequestMapping("/api/reservation")
@CrossOrigin(allowedHeaders = "*")
public class ReservacionController {

    @Autowired
    ReservacionService reservacionService;

    @GetMapping
    public List<Reservacion> getReservations(){
        return this.reservacionService.getReservations();
    }

    @GetMapping("/{numberOfPeople}")
    public List<Reservacion> getReservations(@PathVariable int numberOfPeople){
        return this.reservacionService.getReservations(numberOfPeople);
    }

    @GetMapping("/{name}")
    public List<Reservacion> getReservations(@PathVariable String name){
        return this.reservacionService.getReservations(name);
    }

}
