package si.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import si.model.Mesa;
import si.model.Reservacion;
import si.services.MesaService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/mesa")
@CrossOrigin(allowedHeaders = "*")
public class MesaController {

    @Autowired
    MesaService mesaService;

    @GetMapping
    public HashMap<String,List<Mesa>> getMesas(){
        HashMap<String,List<Mesa>> mesas = new HashMap<>();
        mesas.put("mesas",this.mesaService.getMesas());
        return mesas;
    }

    @GetMapping("/available")
    public HashMap<String,List<Mesa>> getMesas(@RequestParam String date, @RequestParam int numberOfSits){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dt = LocalDateTime.parse(date,dtf);
        HashMap<String,List<Mesa>> mesas = new HashMap<>();
        mesas.put("mesas",this.mesaService.getMesasAvailableAtDateAndNumberOfSits(dt,numberOfSits));
        return mesas;
    }

    @GetMapping("/{numberOfSits}")
    public HashMap<String,List<Mesa>> getMesas(@PathVariable int numberOfSits){
        HashMap<String,List<Mesa>> mesas = new HashMap<>();
        mesas.put("mesas",this.mesaService.getMesas(numberOfSits));
        return mesas;
    }

    @PostMapping("/{tableNumber}")
    public HashMap<String,Mesa> addReservation(@PathVariable int tableNumber, @RequestBody Reservacion reservacion){
        HashMap<String,Mesa> mesas = new HashMap<>();
        mesas.put("mesa",mesaService.addReservation(tableNumber,reservacion));
        return mesas;
    }

}
