package si.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class Mesa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private int tableNumber;

    @Column
    private int numberOfSits;

    @OneToMany(targetEntity = Reservacion.class,cascade = CascadeType.ALL)
    private List<Reservacion> reservacions;

    public Mesa(int tableNumber, int numberOfSits) {
        this.tableNumber = tableNumber;
        this.numberOfSits = numberOfSits;
        this.reservacions = new ArrayList<>();
    }

    public void addReservation(Reservacion reservacion){
       this.reservacions.add(reservacion);
    }

    public boolean containsReservation(LocalDateTime localDate){
        for(Reservacion reservacion: this.reservacions){
            if(reservacion.getDate().equals(localDate)){
                return true;
            }
        }
        return false;
    }

}
