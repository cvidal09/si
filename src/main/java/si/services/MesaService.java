package si.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import si.model.Mesa;
import si.model.Reservacion;
import si.repositories.MesaRepository;
import si.repositories.ReservacionRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MesaService {

    @Autowired
    private MesaRepository mesaRepository;

    @Autowired
    private ReservacionRepository reservacionRepository;

    @Autowired
    private ReservacionService reservacionService;


    public List<Mesa> getMesas(){
        return this.mesaRepository.findAll();
    }

    public List<Mesa> getMesasAvailableAtDateAndNumberOfSits(LocalDateTime date, int numberOfSits){
        List<Mesa> mesasAvailable = new ArrayList<>();
        for(Mesa mesa: getMesas()) {
            if(!mesa.containsReservation(date)&&mesa.getNumberOfSits()== numberOfSits){
                mesasAvailable.add(mesa);
            }
        }
        return mesasAvailable;
    }

     public List<Mesa> getMesas(int numberOfSits){
        return this.mesaRepository.findByNumberOfSits(numberOfSits);
    }

    public Mesa addReservation(int tableNumber, Reservacion reservacion){
        Mesa mesa = getMesa(tableNumber);
        if (mesa.containsReservation(reservacion.getDate())) {
            throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, "Ya se realizo una reservacion para la mesa "+tableNumber+" a las "+ reservacion.getDate().toString());
        }else{
            reservacion = this.reservacionRepository.save(reservacion);
            mesa.addReservation(reservacion);
            return this.mesaRepository.save(mesa);
        }
    }

    private Mesa getMesa(int tableNumber){
        Optional<Mesa> mesa = this.mesaRepository.findByTableNumber(tableNumber);
        if(mesa.isPresent()){
            return mesa.get();
        }else{
            throw new ResponseStatusException(HttpStatus.I_AM_A_TEAPOT, "La mesa no existe");
        }
    }
}
