package si.services;

import si.model.Reservacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.repositories.ReservacionRepository;

import java.util.List;

@Service
public class ReservacionService {

    @Autowired
    private ReservacionRepository reservacionRepository;

    public List<Reservacion> getReservations(int numberOfPeople){
        return this.reservacionRepository.findByNumberOfPeople(numberOfPeople);
    }

    public List<Reservacion> getReservations(){
        return this.reservacionRepository.findAll();
    }

    public List<Reservacion> getReservations(String name){
        return this.reservacionRepository.findByName(name);
    }

}
