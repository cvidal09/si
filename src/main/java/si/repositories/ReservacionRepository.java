package si.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import si.model.Reservacion;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
@Transactional
public interface ReservacionRepository extends JpaRepository<Reservacion, Long> {

    List<Reservacion> findByNumberOfPeople(int numberOfPeople);

    List<Reservacion> findByName(String name);

    boolean existsByNumberOfPeopleAndDate(int numberOfPeople, LocalDate date);
}
