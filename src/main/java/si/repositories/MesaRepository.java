package si.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import si.model.Mesa;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface MesaRepository extends JpaRepository<Mesa, Long> {

    List<Mesa> findByNumberOfSits(int numberOfSits);

    Optional<Mesa> findByTableNumber(int tableNumber);
}
